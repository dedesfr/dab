<?php
session_start();
include("base/koneksi.php");
$page		= "prd";
$pagetree	= "prddocument";

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}

$id = "";
$id = @$_GET['id'];
$ss = "";
$ss = @$_GET['ss'];
$iddoc = "";
$iddoc = @$_GET['iddoc'];

$namaOpr = $_SESSION['nama'];
$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.html"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Sub-product Documents<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Daftar product -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Sub-product</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>product</th>
				  <th>Sub-product</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$prdQ = mysqli_query($con, "select * FROM ms_products s, ms_subproducts ss WHERE s.prd_id = ss.prd_id ORDER BY ss.prd_id ASC");
				while($prd = mysqli_fetch_array($prdQ)){
				?>
				<tr>
				  <td><a href="prd-document.php?id=<?php echo $prd['subprd_id']."&ss=".$prd['subprd_name']; ?>">Add Document</a></td>
                  <td><?php echo $prd['prd_name']; ?></td>
                  <td><?php echo $prd['subprd_name']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Daftar product -->
		
		<!-- Daftar Dokumen -->
		<?php if($id != ""){ ?>
		<div class="col-md-6">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Dokumen <?php echo $ss; ?></h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>Nama Dokumen</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$docQ = mysqli_query($con, "select * FROM ms_subproducts ss, tr_prd_document sd 
				WHERE ss.subprd_id = sd.subprd_id AND ss.subprd_id = $id ORDER BY sd.doc_id ASC");
				while($doc = mysqli_fetch_array($docQ)){
				?>
				<tr>
				  <td><a href="prd-document.php?id=<?php echo $id; ?>&ss=<?php echo $ss; ?>&iddoc=<?php echo $doc['doc_id']; ?>">Edit</a>
				  | <a href="scripts/products-document.php?doc=del&iddoc=<?php echo $doc['doc_id']; ?>">Delete</a></td>
                  <td><?php echo $doc['doc_name']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Daftar Dokumen -->
		
        <!-- tambah Dokumen -->
		<?php if($iddoc == ""){ ?>
		<form enctype="multipart/form-data" action="scripts/products-document.php" method="post">
		<input type="hidden" value="<?php echo $id; ?>" name="idsprd" />
		<input type="hidden" value="new" name="doc" />
		<div class="col-md-6">

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Document</h3>
            </div>
			
              <div class="box-body">
			  
				<div class="form-group col-md-12">
                  <label for="doc_name" class="col-sm-12 control-label">Nama File</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="doc_name">
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="picture" class="col-sm-12 control-label">Upload File</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-document.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php }else{ ?>
		<!-- /tambah Dokumen -->
		
		<!-- Modifikasi Dokumen -->
		<?php
		$editDocQ = mysqli_query($con, "select doc_name FROM  tr_prd_document WHERE doc_id = $iddoc");
		$editDoc = mysqli_fetch_array($editDocQ);
		?>
		<form enctype="multipart/form-data" action="scripts/products-document.php" method="post">
		<input type="hidden" value="<?php echo $id; ?>" name="idsprd" />
		<input type="hidden" value="<?php echo $iddoc; ?>" name="iddoc" />
		<input type="hidden" value="edit" name="doc" />
		<div class="col-md-6">

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Document</h3>
            </div>
			
              <div class="box-body">
			  
				<div class="form-group col-md-12">
                  <label for="doc_name" class="col-sm-12 control-label">Nama File</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="doc_name" value="<?php echo $editDoc['doc_name']; ?>">
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="picture" class="col-sm-12 control-label">Upload File (max 10MB)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-document.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php }} ?>
		<!-- /Modifikasi Dokumen -->
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script>
$(function () {
	$('#example1').DataTable();
	$('#example2').DataTable();
});
</script>
</body>
</html>
