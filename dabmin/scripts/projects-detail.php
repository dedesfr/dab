<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

$type 				= @$_REQUEST['type'];
$pcat_id			= @$_POST['pcat_id'];
$prj_owner			= @$_POST['prj_owner'];
$prj_description	= @$_POST['prj_description'];

if($type == "")
	header("location: ../projects-detail.php?info= - Tipe tidak ditemukan.");
elseif($type == 'add')
{
	if($pcat_id == "" || $prj_owner == "" || $prj_description == "")
		header("location: ../projects-detail.php?info= - Semua field harus diisi.");
	else
	{
		$insert = mysqli_query($con, "INSERT INTO tr_project (pcat_id, prj_owner, prj_description) 
		VALUES ('$pcat_id', '$prj_owner', '$prj_description')");
		if($insert == 1)
			header("location: ../projects-detail.php?info= - Project sukses ditambah");
		else
			header("location: ../projects-detail.php?info= - Project gagal ditambah");
	}
	
}
elseif($type == 'edit')
{	
	$idPrj = @$_POST['idPrj'];
	
	if($pcat_id == "" || $prj_owner == "" || $prj_description == "")
		header("location: ../projects-detail.php?info= - Semua field harus diisi.");
	elseif($idPrj == "")
		header("location: ../projects-detail.php?info= - ID Project tidak ditemukan.");
	else
	{
		$update = mysqli_query($con, "UPDATE tr_project SET pcat_id = '$pcat_id', prj_owner = '$prj_owner', prj_description = '$prj_description' WHERE prj_id = '$idPrj'");
		if($update == 1)
			header("location: ../projects-detail.php?info= - Project sukses diedit");
		else
			header("location: ../projects-detail.php?info= - Project gagal diedit");
	}
}
elseif($type == 'del')
{
	$idPrj = @$_REQUEST['idPrj'];

	$delete = mysqli_query($con, "DELETE FROM tr_project WHERE prj_id = '$idPrj'");
	if(!$delete)
		header("location:../projects-detail.php?info= - Project gagal dihapus");
	else
		header("location:../projects-detail.php?info= - Project sukses dihapus");
}

mysqli_close($con);
?>