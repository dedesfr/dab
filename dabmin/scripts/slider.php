<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

$id			= @$_REQUEST['id'];
$sli_text1	= @$_REQUEST['sli_text1'];
$sli_text2	= @$_REQUEST['sli_text2'];
$sli_text3	= @$_REQUEST['sli_text3'];

if($id == "")
	header("location:../dashboard.php?info= - ID tidak ditemukan");
else
{
	if($id == "x")
	{
		$filenyah 	= @$_FILES["file"]["name"];
		
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 5000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../dashboard.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{
					$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'ms_slider'")); 
					$curID = $cekID["AUTO_INCREMENT"];
					
					$UploadKemanaFilenya1 	= "../../gambar/slider/";
					$UploadKemanaFilenya2 	= "gambar/slider/";
					$fileName 				= "sli_".$curID.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{
						//resize
						$image = new SimpleImage();
						$image->load($filePath);
						$image->resize(800,520);
						$image->save($filePath);
						
						$insert = mysqli_query($con, "INSERT INTO ms_slider (sli_text1, sli_text2, sli_image) 
						VALUES ('$sli_text1', '$sli_text2', '$filePath3')");
						if($insert == 1)
							header("location: ../dashboard.php?info= - Slider sukses ditambah");
					}
					else
					  header("location: ../dashboard.php?info= - Slider gagal ditambah");
				}
			}
			else
				header("location: ../dashboard.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 300Kb.");
		}
		else
			header("location: ../dashboard.php?info= - Image harus diisi.");
	}
	else
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 5000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../dashboard.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{			
					$UploadKemanaFilenya1 	= "../../gambar/slider/";
					$UploadKemanaFilenya2 	= "gambar/slider/";
					$fileName 				= "sli_".$id.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{
						//resize
						$image = new SimpleImage();
						$image->load($filePath);
						$image->resize(800,520);
						$image->save($filePath);
						
						$update = mysqli_query($con, "UPDATE ms_slider SET sli_text1 = '$sli_text1', sli_text2 = '$sli_text2',
								sli_image = '$filePath3' WHERE sli_id = '$id'");
						if($update == 1)
							header("location: ../dashboard.php?info= - Slider sukses diupdate");
					}
					else
					  header("location: ../dashboard.php?info= - Slider gagal diupdate");
				}
			}
			else
				header("location: ../dashboard.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
		}
		else
		{	
			$update = mysqli_query($con, "UPDATE ms_slider SET sli_text1 = '$sli_text1', sli_text2 = '$sli_text2'
									WHERE sli_id = '$id'");
			if($update == 1)
				header("location:../dashboard.php?info= - Slider sukses diupdate");
			else
				header("location:../dashboard.php?info= - Slider gagal diupdate");
		}
	}
}
mysqli_close($con);
?>