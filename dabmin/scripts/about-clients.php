<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

$part_id		= @$_REQUEST['id'];
$part_status	= @$_REQUEST['part_status'];

if($part_id == "")
	header("location:../about-clients.php?info= - ID tidak ditemukan.");
else
{
	if($part_id == "x")
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "" && $part_status != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 200000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../about-clients.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 200Kb.");
				else
				{
					$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'tr_clients'")); 
					$curID = $cekID["AUTO_INCREMENT"];
					
					$UploadKemanaFilenya1 	= "../../gambar/about/";
					$UploadKemanaFilenya2 	= "gambar/about/";
					$fileName 				= "cert_".$curID.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{
						//resize
						$image = new SimpleImage();
						$image->load($filePath);
						$image->resize(291,414);
						$image->save($filePath);
						
						$update = mysqli_query($con, "INSERT INTO tr_clients (part_picture, part_status) 
						VALUES ('$filePath3', '$part_status')");
						if($update == 1)
							header("location: ../about-clients.php?info= - Client sukses ditambah");
						else
							header("location: ../about-clients.php?info= - Client gagal ditambah");
					}
					else
						header("location: ../about-clients.php?info= - Client gagal ditambah");
				}
			}
			else
				header("location: ../about-clients.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 200Kb.");
		}
		else
			header("location: ../about-clients.php?info= - Semua field harus diisi.");
	}
	else
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "" && $part_status != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 200000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../about-clients.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 200Kb.");
				else
				{
					$UploadKemanaFilenya1 	= "../../gambar/about/";
					$UploadKemanaFilenya2 	= "gambar/about/";
					$fileName 				= "cert_".$part_id.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{
						//resize
						$image = new SimpleImage();
						$image->load($filePath);
						$image->resize(291,414);
						$image->save($filePath);
						
						$update = mysqli_query($con, "UPDATE tr_clients SET part_picture = '$filePath3', part_status = '$part_status' WHERE part_id = '$part_id'");
						if($update == 1)
							header("location: ../about-clients.php?info= - Client sukses diupdate");
					}
					else
						header("location: ../about-clients.php?info= - Client gagal diupdate");
				}
			}
			else
				header("location: ../about-clients.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 200Kb.");
		}
		elseif($filenyah == "" && $part_status != "")
		{
			$update = mysqli_query($con, "UPDATE tr_clients SET part_status = '$part_status' WHERE part_id = '$part_id'");
			if($update == 1)
				header("location: ../about-clients.php?info= - Client sukses diupdate");
			else
				header("location: ../about-clients.php?info= - Client gagal diupdate");
		}
		else
			header("location: ../about-clients.php?info= - Semua field harus diisi.");
	}
}

mysqli_close($con);
?>