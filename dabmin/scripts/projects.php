<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

$type = @$_REQUEST['type'];

if($type == 'add')
{
	$pcat_name	= @$_REQUEST['pcat_name'];
	$filenyah 	= @$_FILES["file"]["name"];
	
	if($filenyah != "" && $pcat_name != "")
	{
		$allowedExts = array("jpg", "jpeg", "png");
		$extension = end(explode(".", $_FILES["file"]["name"]));
		
		if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000) && in_array($extension, $allowedExts))
		{
			if ($_FILES["file"]["error"] > 0)
				header("location: ../projects.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
			else
			{
				$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'ms_project_category'")); 
				$curID = $cekID["AUTO_INCREMENT"];
				
				$UploadKemanaFilenya1 	= "../../gambar/projects/";
				$UploadKemanaFilenya2 	= "gambar/projects/";
				$fileName 				= "pcat_".$curID.".".$extension;
				$tmpFile				= $_FILES["file"]["tmp_name"];
				$filePath 				= $UploadKemanaFilenya1.$fileName;
				$filePath3				= $UploadKemanaFilenya2.$fileName;
				
				$simpen = move_uploaded_file($tmpFile, $filePath);	  
				if($simpen == 1)
				{
					//resize
					$image = new SimpleImage();
					$image->load($filePath);
					$image->resize(261,280);
					$image->save($filePath);
						
					$update = mysqli_query($con, "INSERT INTO ms_project_category (pcat_name, pcat_picture) 
					VALUES ('$pcat_name', '$filePath3')");
					if($update == 1)
						header("location: ../projects.php?info= - Project sukses ditambah");
					else
						header("location: ../projects.php?info= - Project gagal ditambah");
				}
				else
					header("location: ../projects.php?info= - Project gagal ditambah");
			}
		}
		else
			header("location: ../projects.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
	}
	else
		header("location: ../projects.php?info= - Semua field harus diisi.");
}
elseif($type == 'edit')
{
	$idPrj		= @$_REQUEST['idPrj'];
	$pcat_name	= @$_REQUEST['pcat_name'];
	$filenyah 	= @$_FILES["file"]["name"];
	
	if($idPrj == "")
		header("location: ../projects.php?info= - ID Project tidak ditemukan.");
	elseif($filenyah != "" && $pcat_name != "")
	{
		$allowedExts = array("jpg", "jpeg", "png");
		$extension = end(explode(".", $_FILES["file"]["name"]));
		
		if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000) && in_array($extension, $allowedExts))
		{
			if ($_FILES["file"]["error"] > 0)
				header("location: ../projects.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
			else
			{				
				$UploadKemanaFilenya1 	= "../../gambar/projects/";
				$UploadKemanaFilenya2 	= "gambar/projects/";
				$fileName 				= "pcat_".$idPrj.".".$extension;
				$tmpFile				= $_FILES["file"]["tmp_name"];
				$filePath 				= $UploadKemanaFilenya1.$fileName;
				$filePath3				= $UploadKemanaFilenya2.$fileName;
				
				$simpen = move_uploaded_file($tmpFile, $filePath);	  
				if($simpen == 1)
				{
					$image = new SimpleImage();
					$image->load($filePath);
					$image->resize(261,280);
					$image->save($filePath);
					
					$update = mysqli_query($con, "UPDATE ms_project_category SET pcat_name = '$pcat_name', pcat_picture = '$filePath3' WHERE pcat_id = '$idPrj'");
					if($update == 1)
						header("location: ../projects.php?info= - Project sukses diedit");
					else
						header("location: ../projects.php?info= - Project gagal diedit");
				}
				else
					header("location: ../projects.php?info= - Project gagal diedit");
			}
		}
		else
			header("location: ../projects.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
	}
	elseif($filenyah == "" && $pcat_name != "")
	{
		$update = mysqli_query($con, "UPDATE ms_project_category SET pcat_name = '$pcat_name' WHERE pcat_id = '$idPrj'");
		if($update == 1)
			header("location: ../projects.php?info= - Project sukses diedit");
		else
			header("location: ../projects.php?info= - Project gagal diedit");
	}
	else
		header("location: ../projects.php?info= - Nama Kategori harus diisi.");
}
elseif($type == 'del')
{
	$idPrj = @$_REQUEST['idPrj'];
	
	$cekSubQ = mysqli_query($con, "SELECT COUNT(*) AS total FROM tr_project WHERE pcat_id = '$idPrj'");
	$cekSub = mysqli_fetch_assoc($cekSubQ);
	$num_rows = $cekSub['total'];
	
	if($num_rows > 0)
		header("location:../projects.php?info= - Project Category ini masih ada isinya, silahkan hapus isinya terlebih dahulu");
	else
	{
		$cekFileQuery 	= mysqli_query($con, "SELECT pcat_picture FROM ms_project_category 
		WHERE pcat_id = '$idPrj'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['pcat_picture'];

		unlink("../../".$cekFilePic);

		$delete = mysqli_query($con, "DELETE FROM ms_project_category WHERE pcat_id = '$idPrj'");
		if(!$delete)
			header("location:../projects.php?info= - Project Category gagal dihapus");
		else
			header("location:../projects.php?info= - Project Category sukses dihapus");
	}
}

mysqli_close($con);
?>