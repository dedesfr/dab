<?php
session_start();
include "../base/koneksi.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$id  	= @$_REQUEST['id'];
	
if($id == "")
	header("location:../about-story.php?info= - ID tidak boleh kosong.");
else
{
	try
	{
		start();
		
		$cekFileQuery 	= mysqli_query($con, "SELECT n_picture FROM tr_story WHERE n_id = '$id'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['n_picture'];

		//unlink($cekFilePic);
		unlink("../../".$cekFilePic);
		
		$delete = mysqli_query($con, "DELETE FROM tr_story WHERE n_id = '$id'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../about-story.php?info= - Delete Content berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../about-story.php?info= - Delete Content gagal.");
	}
}

mysqli_close($con);
?>