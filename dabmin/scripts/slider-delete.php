<?php
session_start();
include "../base/koneksi.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$id	= @$_REQUEST['id'];

if($id == "")
	header("location:../dashboard.php?info= - ID tidak ditemukan");
else
{
	try
	{
		start();
		
		$cekFileQuery 	= mysqli_query($con, "SELECT sli_image FROM ms_slider WHERE sli_id = '$id'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['sli_image'];

		//unlink($cekFilePic);
		unlink("../../".$cekFilePic);
		
		$delete = mysqli_query($con, "DELETE FROM ms_slider WHERE sli_id = '$id'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../dashboard.php?info= - Delete Slider sukses.");
	}
	catch(Exception $e){
		rollback();
		header("location:../dashboard.php?info= - Delete Slider gagal.");
	}	
}

mysqli_close($con);
?>