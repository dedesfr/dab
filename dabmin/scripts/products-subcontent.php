<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

$idprd				= @$_REQUEST['idsprd'];
$prd_content 		= @$_REQUEST['subprd_content'];

if($idprd == "")
	header("location:../prd-subcontent.php?info= - ID tidak ditemukan.");
else
{
	$filenyah 	= @$_FILES["file"]["name"];
	
	if($filenyah != "" && $prd_content != "")
	{
		$allowedExts = array("jpg", "jpeg", "png");
		$extension = end(explode(".", $_FILES["file"]["name"]));
		
		if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000) && in_array($extension, $allowedExts))
		{
			if ($_FILES["file"]["error"] > 0)
				header("location: ../prd-subcontent.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
			else
			{
				$UploadKemanaFilenya1 	= "../../gambar/subproduct/";
				$UploadKemanaFilenya2 	= "gambar/subproduct/";
				$fileName 				= "sprd_".$idprd.".".$extension;
				$tmpFile				= $_FILES["file"]["tmp_name"];
				$filePath 				= $UploadKemanaFilenya1.$fileName;
				$filePath3				= $UploadKemanaFilenya2.$fileName;
				
				$simpen = move_uploaded_file($tmpFile, $filePath);	  
				if($simpen == 1)
				{
					//resize
					$image = new SimpleImage();
					$image->load($filePath);
					$image->resize(770,435);
					$image->save($filePath);
					
					$prd_content = str_replace("../gambar/prdSubcontent/","gambar/prdSubcontent/",$prd_content);
					$update = mysqli_query($con, "UPDATE ms_subproducts SET 
					subprd_picture = '$filePath3',
					subprd_content = '$prd_content'
					WHERE subprd_id = '$idprd'");
					if($update == 1)
						header("location: ../prd-subcontent.php?info= - product Content sukses diupdate");
					else
						header("location: ../prd-subcontent.php?info= - product Content gagal diupdate");
				}
				else
					header("location: ../prd-subcontent.php?info= - product Content gagal diupdate");
			}
		}
		else
			header("location: ../prd-subcontent.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
	}
	elseif($filenyah == "" && $prd_content != "")
	{
		$prd_content = str_replace("../gambar/prdSubcontent/","gambar/prdSubcontent/",$prd_content);
		$update = mysqli_query($con, "UPDATE ms_subproducts SET 
		subprd_content = '$prd_content' WHERE subprd_id = '$idprd'");
		if($update == 1)
			header("location: ../prd-subcontent.php?info= - product Content sukses diupdate");
		else
			header("location: ../prd-subcontent.php?info= - product Content gagal diupdate");
	}
	else
		header("location: ../prd-subcontent.php?info= - product Header dan Content harus diisi.");
}

mysqli_close($con);
?>