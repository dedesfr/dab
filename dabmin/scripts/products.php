<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$prd = @$_REQUEST['prd'];

if($prd == 'product')
{
	$idprd		= @$_REQUEST['idprd'];
	$prd_name 	= @$_REQUEST['prd_name'];
	$prd_type 	= @$_REQUEST['prd_type'];
	$prd_picture= @$_REQUEST['prd_picture'];
	
	if($idprd == "" || $prd_name == "")
		header("location:../prd-category.php?info= - Semua field harus diisi.");
	else
	{
		if($idprd == "x")
		{
			$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'ms_products'")); 
			$curID = $cekID["AUTO_INCREMENT"];
			$filenyah 	= @$_FILES["file"]["name"];
			
			if($filenyah != "")
			{
				$allowedExts = array("jpg", "jpeg", "png");
				$extension = end(explode(".", $_FILES["file"]["name"]));
				
				if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000) && in_array($extension, $allowedExts))
				{
					if ($_FILES["file"]["error"] > 0)
						header("location: ../prd-subcontent.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
					else
					{
						$UploadKemanaFilenya1 	= "../../gambar/product/";
						$UploadKemanaFilenya2 	= "gambar/product/";
						$fileName 				= "prd_".$curID.".".$extension;
						$tmpFile				= $_FILES["file"]["tmp_name"];
						$filePath 				= $UploadKemanaFilenya1.$fileName;
						$filePath3				= $UploadKemanaFilenya2.$fileName;
						
						$simpen = move_uploaded_file($tmpFile, $filePath);	  
						if($simpen == 1)
						{
							//resize
							$image = new SimpleImage();
							$image->load($filePath);
							$image->resize(300,300);
							$image->save($filePath);
							
							$insert = mysqli_query($con, "INSERT INTO ms_products (prd_name, prd_type, prd_picture) VALUES ('$prd_name', '$prd_type', '$filePath3')");
							if($insert == 1)
								header("location: ../prd-category.php?info= - Product sukses ditambah");
							else
								header("location: ../prd-category.php?info= - Product gagal ditambah");
						}
						else
							header("location: ../prd-category.php?info= - product Content gagal diupdate");
					}
				}
				else
					header("location: ../prd-category.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
			}

			else
			{
				$insert = mysqli_query($con, "INSERT INTO ms_products (prd_name, prd_type) VALUES ('$prd_name', '$prd_type')");
				if($insert == 1)
					header("location: ../prd-category.php?info= - Product sukses ditambah");
				else
					header("location: ../prd-category.php?info= - Product gagal ditambah");

			}
		}
		else
		{
			$filenyah 	= @$_FILES["file"]["name"];
			
			if($filenyah != "")
			{
				$allowedExts = array("jpg", "jpeg", "png");
				$extension = end(explode(".", $_FILES["file"]["name"]));
				
				if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000) && in_array($extension, $allowedExts))
				{
					if ($_FILES["file"]["error"] > 0)
						header("location: ../prd-subcontent.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
					else
					{
						$UploadKemanaFilenya1 	= "../../gambar/product/";
						$UploadKemanaFilenya2 	= "gambar/product/";
						$fileName 				= "prd_".$idprd.".".$extension;
						$tmpFile				= $_FILES["file"]["tmp_name"];
						$filePath 				= $UploadKemanaFilenya1.$fileName;
						$filePath3				= $UploadKemanaFilenya2.$fileName;
						
						$simpen = move_uploaded_file($tmpFile, $filePath);	  
						if($simpen == 1)
						{
							//resize
							$image = new SimpleImage();
							$image->load($filePath);
							$image->resize(300,300);
							$image->save($filePath);
							
							$update = mysqli_query($con, "UPDATE ms_products SET prd_name = '$prd_name', prd_type = '$prd_type', prd_picture = '$filePath3' WHERE prd_id = '$idprd'");
							if($update == 1)
								header("location: ../prd-category.php?info= - Product sukses diupdate");
							else
							header("location: ../prd-category.php?info= - Product gagal diupdate");
						}
						else
							header("location: ../prd-category.php?info= - product Content gagal diupdate");
					}
				}
				else
					header("location: ../prd-category.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 500Kb.");
			}

			else
			{
				$update = mysqli_query($con, "UPDATE ms_products SET prd_name = '$prd_name', prd_type = '$prd_type' WHERE prd_id = '$idprd'");
				if($update == 1)
					header("location: ../prd-category.php?info= - Product sukses diupdate");
				else
				header("location: ../prd-category.php?info= - Product gagal diupdate");

			}
		}				
	}
}
elseif($prd == 'subproduct')
{
	$idsprd			= @$_REQUEST['idsprd'];
	$prd_id 		= @$_REQUEST['prd_id'];
	$subprd_name	= @$_REQUEST['subprd_name'];

	if($idsprd == "" || $prd_id == "" || $subprd_name == "")
		header("location:../prd-category.php?info= - Semua field harus diisi.");
	else
	{
		if($idsprd == "x")
		{			
			$insert = mysqli_query($con, "INSERT INTO ms_subproducts (prd_id, subprd_name) VALUES ('$prd_id', '$subprd_name')");
			if($insert == 1)
				header("location:../prd-category.php?info= - Sub-Product sukses ditambah");
			else
				header("location:../prd-category.php?info= - Sub-Product gagal ditambah");
		}
		else
		{
			$update = mysqli_query($con, "UPDATE ms_subproducts SET prd_id = '$prd_id', subprd_name = '$subprd_name' WHERE subprd_id = '$idsprd'");
			if($update == 1)
				header("location:../prd-category.php?info= - Sub-Product sukses diupdate");
			else
				header("location:../prd-category.php?info= - Sub-Product gagal diupdate");
		}
	}
}
elseif($prd == 'delprd')
{
	$idprd = @$_REQUEST['idprd'];

	try
	{
		start();

		$cekFileQuery 	= mysqli_query($con, "SELECT prd_picture FROM ms_products WHERE prd_id = '$idprd'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['prd_picture'];

		//unlink($cekFilePic);
		unlink("../../".$cekFilePic);
		
		$cekSubQ = mysqli_query($con, "SELECT COUNT(*) AS total FROM ms_subproducts WHERE prd_id = '$idprd'");
		$cekSub = mysqli_fetch_assoc($cekSubQ);
		$num_rows = $cekSub['total'];
		
		if($num_rows > 0)
		{
			$delete = mysqli_query($con, "DELETE FROM ms_subproducts WHERE prd_id = '$idprd'");
			if(!$delete)
				throw new Exception("gagal");
		}
		
		$delete2 = mysqli_query($con, "DELETE FROM ms_products WHERE prd_id = '$idprd'");
		if(!$delete2)
			throw new Exception("gagal");

		commit();
		header("location:../prd-category.php?info= - Delete Product berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../prd-category.php?info= - Delete Product gagal.");
	}	
}
elseif($prd == 'delsprd')
{
	$idsprd = @$_REQUEST['idsprd'];

	try
	{
		start();
		
		$delete = mysqli_query($con, "DELETE FROM ms_subproducts WHERE subprd_id = '$idsprd'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../prd-category.php?info= - Delete Sub-Product berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../prd-category.php?info= - Delete Sub-Product gagal.");
	}	
}

mysqli_close($con);
?>