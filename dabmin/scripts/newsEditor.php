<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";
date_default_timezone_set("Asia/Jakarta");

$hiddenType = @$_REQUEST['hiddentype'];
$id			= @$_REQUEST['id'];
$Header		= @$_REQUEST['Header'];
$nContent	= @$_REQUEST['nContent'];

$Tanggal = new DateTime($_REQUEST['Tanggal']);
$Tanggal = $Tanggal->format('Y-m-d');

if($id == "")
	header("location:../news.php?info= - ID tidak ditemukan");
else
{
	if($id == "x")
	{
		$filenyah 	= @$_FILES["file"]["name"];
		
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 300000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../news.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 300Kb.");
				else
				{
					$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'tr_news'")); 
					$curID = $cekID["AUTO_INCREMENT"];
							
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "news_".$curID.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{						
						$insert = mysqli_query($con, "INSERT INTO tr_news (n_header, n_date, n_content, n_picture) 
						VALUES ('$Header', '$Tanggal', '$nContent', '$filePath3')");
						if($insert == 1)
							header("location: ../news.php?info= - Content sukses ditambah");
					}
					else
					  header("location: ../news.php?info= - Content gagal ditambah");
				}
			}
			else
				header("location: ../news.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 300Kb.");
		}
		else
			header("location: ../news.php?info= - Image harus diisi.");
	}
	else
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 300000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../news.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 300Kb.");
				else
				{			
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "news_".$id.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{						
						$update = mysqli_query($con, "UPDATE tr_news SET n_header = '$Header', n_date = '$Tanggal', n_content = '$nContent', n_picture = '$filePath3' 
						WHERE n_id = '$id'");
						if($update == 1)
							header("location: ../news.php?info= - News sukses diupdate");
					}
					else
					  header("location: ../news.php?info= - News gagal diupdate");
				}
			}
			else
				header("location: ../news.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 300Kb.");
		}
		else
		{	
			$update = mysqli_query($con, "UPDATE tr_news SET n_header = '$Header', n_date = '$Tanggal', n_content = '$nContent' WHERE n_id = '$id'");
			if($update == 1)
				header("location:../news.php?info= - News sukses diupdate");
			else
				header("location:../news.php?info= - News gagal diupdate");
		}
	}
}
mysqli_close($con);
?>