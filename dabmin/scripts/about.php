<?php
session_start();
include "../base/koneksi.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$about = @$_REQUEST['about'];

if($about == 'kategori')
{
	$idabt		= @$_REQUEST['idabt'];
	$abt_name 	= @$_REQUEST['abt_name'];
	
	if($idabt == "" || $abt_name == "")
		header("location:../about-category.php?info= - Semua field harus diisi.");
	else
	{
		if($idabt == "x")
		{
			$insert = mysqli_query($con, "INSERT INTO ms_about (abt_name) VALUES ('$abt_name')");
			if($insert == 1)
				header("location: ../about-category.php?info= - Kategori sukses ditambah");
			else
				header("location: ../about-category.php?info= - Kategori gagal ditambah");
		}
		else
		{
			$update = mysqli_query($con, "UPDATE ms_about SET abt_name = '$abt_name' WHERE abt_id = '$idabt'");
			if($update == 1)
				header("location: ../about-category.php?info= - Kategori sukses diupdate");
			else
			  header("location: ../about-category.php?info= - Kategori gagal diupdate");
		}				
	}
}
elseif($about == 'subkategori')
{
	$idsabt			= @$_REQUEST['idsabt'];
	$abt_id 		= @$_REQUEST['abt_id'];
	$subabt_name	= @$_REQUEST['subabt_name'];

	if($idsabt == "" || $abt_id == "" || $subabt_name == "")
		header("location:../about-category.php?info= - Semua field harus diisi.");
	else
	{
		if($idsabt == "x")
		{			
			$insert = mysqli_query($con, "INSERT INTO ms_subabout (abt_id, subabt_name) VALUES ('$abt_id', '$subabt_name')");
			if($insert == 1)
				header("location:../about-category.php?info= - Sub Kategori sukses ditambah");
			else
				header("location:../about-category.php?info= - Sub Kategori gagal ditambah");
		}
		else
		{
			$update = mysqli_query($con, "UPDATE ms_subabout SET abt_id = '$abt_id', subabt_name = '$subabt_name' WHERE subabt_id = '$idsabt'");
			if($update == 1)
				header("location:../about-category.php?info= - Sub Kategori sukses diupdate");
			else
				header("location:../about-category.php?info= - Sub Kategori gagal diupdate");
		}
	}
}
elseif($about == 'delabt')
{
	$idabt = @$_REQUEST['idabt'];

	try
	{
		start();
		
		$cekSubQ = mysqli_query($con, "SELECT COUNT(*) AS total FROM ms_subabout WHERE abt_id = '$idabt'");
		$cekSub = mysqli_fetch_assoc($cekSubQ);
		$num_rows = $cekSub['total'];
		
		if($num_rows > 0)
		{
			$delete = mysqli_query($con, "DELETE FROM ms_subabout WHERE abt_id = '$idabt'");
			if(!$delete)
				throw new Exception("gagal");
		}
		
		$delete2 = mysqli_query($con, "DELETE FROM ms_about WHERE abt_id = '$idabt'");
		if(!$delete2)
			throw new Exception("gagal");

		commit();
		header("location:../about-category.php?info= - Delete Kategori berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../about-category.php?info= - Delete Kategori gagal.");
	}	
}
elseif($about == 'delsabt')
{
	$idsabt = @$_REQUEST['idsabt'];

	try
	{
		start();
		
		$delete = mysqli_query($con, "DELETE FROM ms_subabout WHERE subabt_id = '$idsabt'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../about-category.php?info= - Delete Sub Kategori berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../about-category.php?info= - Delete Sub Kategori gagal.");
	}	
}

mysqli_close($con);
?>