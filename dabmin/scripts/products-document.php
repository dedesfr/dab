<?php
session_start();
include "../base/koneksi.php";

$type		= @$_REQUEST['doc'];
$idsprd		= @$_REQUEST['idsprd'];
$iddoc		= @$_REQUEST['iddoc'];
$doc_name 	= @$_REQUEST['doc_name'];
$filenyah 	= $_FILES["file"]["tmp_name"];

if($type == "new")
{
	if($filenyah != "" && $idsprd != "" && $doc_name != "")
	{
		$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'tr_prd_document'")); 
		$curID = $cekID["AUTO_INCREMENT"];
		
		$extension = end(explode(".", $_FILES["file"]["name"]));
		
		$namaFile	= "sprd_".$idsprd."_".$curID.".".$extension;
		$namaDB		= "upload/".$namaFile;
		
		if(is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$uploadfile = "../../upload/".$namaFile;
			if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile))
			{
				$insertDB = mysqli_query($con, "INSERT INTO tr_prd_document (subprd_id, doc_name, doc_file) 
				VALUES ('$idsprd', '$doc_name', '$namaDB')");
				if($insertDB == 1)
					header("location: ../prd-document.php?info= - Dokumen sukses diupload");
				else
					header("location: ../prd-document.php?info= - Dokumen gagal diupload");
			}
			else
				header("location: ../prd-document.php?info= - Dokumen gagal diupload");
		}
		else
			header("location: ../prd-document.php?info= - Dokumen gagal diupload");
	}
	else
		header("location: ../prd-document.php?info= - Semua field harus diisi");
}
elseif($type == "edit")
{
	if($filenyah != "" && $idsprd != "" && $doc_name != "" && $iddoc != "")
	{
		$extension = end(explode(".", $_FILES["file"]["name"]));
		
		$namaFile	= "sprd_".$idsprd."_".$iddoc.".".$extension;
		$namaDB		= "upload/".$namaFile;
		
		if(is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$uploadfile = "../../upload/".$namaFile;
			if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile))
			{
				$update = mysqli_query($con, "UPDATE tr_prd_document SET 
										doc_name = '$doc_name', doc_file = '$namaDB' WHERE doc_id = '$iddoc'");
				if($update == 1)
					header("location: ../prd-document.php?info= - Dokumen sukses diupdate");
				else
					header("location: ../prd-document.php?info= - Dokumen gagal diupdate");
			}
			else
				header("location: ../prd-document.php?info= - Dokumen gagal diupdate");
		}
		else
			header("location: ../prd-document.php?info= - Dokumen gagal diupdate");
	}
	elseif($filenyah == "" && $idsprd != "" && $doc_name != "" && $iddoc != "")
	{
		$update = mysqli_query($con, "UPDATE tr_prd_document SET doc_name = '$doc_name' WHERE doc_id = '$iddoc'");
		if($update == 1)
			header("location: ../prd-document.php?info= - Dokumen sukses diupdate");
		else
			header("location: ../prd-document.php?info= - Dokumen gagal diupdate");
	}
	else
		header("location: ../prd-document.php?info= - Semua field harus diisi");
}
elseif($type == "del")
{
	if($iddoc != "")
	{
		$cekFileQuery 	= mysqli_query($con, "SELECT doc_file FROM tr_prd_document WHERE doc_id = '$iddoc'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['doc_file'];

		//unlink($cekFilePic);
		unlink("../../".$cekFilePic);

		$DeleteDB = mysqli_query($con, "DELETE FROM tr_prd_document WHERE doc_id = '$iddoc'");
		if($DeleteDB == 1)
			header("location: ../prd-document.php?info= - Dokumen sukses dihapus");
		else
			header("location: ../prd-document.php?info= - Dokumen gagal dihapus");
	}
	else
		header("location:../prd-document.php?info= - ID tidak ditemukan");
}

mysqli_close($con);
?>