<?php
session_start();
include("base/koneksi.php");
$page		= "about";
$pagetree	= "";

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
    $_SESSION['error'] = "Silahkan login terlebih dahulu";
    header("location:index.php");
}

$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DAB Administrator</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="base/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <script src="http://cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include "base/header.html"; ?>
<?php include "base/sidebar.html"; ?>

<div class="content-wrapper">
    <section class="content-header">
    <h1>About Us and Contact Us<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
    <div class="row">
    
        <!-- Modifikasi Contact -->
        <?php
        $editAboutQ = mysqli_query($con, "select * FROM ms_text WHERE txt_id = 1");
        $editAbout = mysqli_fetch_array($editAboutQ);
        $editAbout['txt_about_content'] = str_replace("gambar/aboutContent/","../gambar/aboutContent/",$editAbout['txt_about_content']);
        ?>
        <form enctype="multipart/form-data" action="scripts/about-us.php" method="post">
        <input type="hidden" value="<?php echo $id; ?>" name="idssrv" />
        <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
            <h3 class="box-title">Edit Contact</h3>
            </div>
            
            <div class="box-body">
            
                <div class="form-group col-md-6">
                <label for="abt_name" class="col-sm-12 control-label">Nama PT</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="abt_name" value="<?php echo $editAbout['txt_contact_name']; ?>">
                </div>
                </div>
                
                <div class="form-group col-md-6">
                <label for="abt_name" class="col-sm-12 control-label">Email Penerima Contact Form</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="abt_email" value="<?php echo $editAbout['txt_email']; ?>">
                </div>
                </div>
                
                <div class="form-group col-md-12">
                <label for="abt_content" class="col-sm-12 control-label">iFrame Google Map<br>
                <small>buka maps.google.com > pilih lokasi > share > embed map > ganti size ke Large > copy iFramenya</small></label>
                <div class="col-sm-12">
                    <textarea name="abt_map" id="editor1" rows="6" style="width:100%"><?php echo $editAbout['txt_map']; ?></textarea>
                </div>
                </div>
                
                <div class="form-group col-md-12">
                <label for="abt_content" class="col-sm-12 control-label">Content</label>
                <div class="col-sm-12">
                    <textarea name="abt_content" id="editor2" rows="10" cols="80"><?php echo $editAbout['txt_contact_content']; ?></textarea>
                </div>
                </div>

                <div class="form-group col-md-6">
                <label for="abt_name" class="col-sm-12 control-label">Judul</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="abt_judul" value="<?php echo $editAbout['txt_about_judul']; ?>">
                </div>
                </div>

                <div class="form-group col-md-12">
                    <label for="picture" class="col-sm-12 control-label">Picture</label>
                    <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                    </div>
                </div>

                <div class="form-group col-md-12">
                <label for="abt_content" class="col-sm-12 control-label">About Content</label>
                <div class="col-sm-12">
                    <textarea name="abt_about_content" id="editor3" rows="10" cols="80"><?php echo $editAbout['txt_about_content']; ?></textarea>
                </div>
                </div>

            </div>
            
            <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="contact.php" class="btn btn-default">Reset</a>
            </div>
            
        </div>
        </div>
        </form>
        <!-- /Modifikasi Contact -->
        
    </div>
    </section>
</div>
<?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script>
$(function () {
    CKEDITOR.replace( 'editor2' );
	CKEDITOR.replace( 'editor3', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload-about-us.php",
	} );
});
</script>
</body>
</html>
