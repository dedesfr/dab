<?php
session_start();
include("base/koneksi.php");
$page 		= "about";
$pagetree	= "aboutclients";

$idadmin = $_SESSION['idadmin'];

if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$id = "";
$id = @$_GET['id'];

$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "base/header.html"; ?>
  <?php include "base/sidebar.html"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Clients<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Clients -->
		<div class="col-md-6">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Clients <small>(refresh halaman ini jika Image belum berubah)</small></h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>Picture</th>
				  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$partnersQ = mysqli_query($con, "select * FROM tr_clients ORDER BY part_id ASC");
				while($partners = mysqli_fetch_array($partnersQ)){
				?>
				<tr>
				  <td><a href="about-clients.php?id=<?php echo $partners['part_id']; ?>">Edit</a></td>
                  <td><img src="../<?php echo $partners['part_picture']; ?>" height="90px" /></td>
				  <td><?php echo $partners['part_status']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Clients -->
        
        <!-- tambah Clients -->
		<?php if($id == ""){ ?>
		<form enctype="multipart/form-data" action="scripts/about-clients.php" method="post">
		<input type="hidden" value="x" name="id" />
		<div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Client</h3>
            </div>
              <div class="box-body">
				
				<div class="form-group col-md-6">
                  <label for="Content" class="col-sm-12 control-label">Picture (Picture otomatis di resize ke 291x414px)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				<div class="form-group col-md-6">
					<label for="part_status" class="col-sm-12 control-label">Status</label>
					<div class="col-sm-12">
						<select class="form-control" name="part_status">
							<option value="Active">Active</option>
							<option value="Inactive">Inactive</option>
						</select>
					</div>
                </div>
				
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="about-clients.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /tambah Clients -->
		
		<!-- Modifikasi Clients -->
		<?php
		if($id != ""){
		$editPartnersQ = mysqli_query($con, "select * FROM tr_clients WHERE part_id = $id");
		$editPartners = mysqli_fetch_array($editPartnersQ);
		$status = $editPartners['part_status'];
		?>
		<form enctype="multipart/form-data" action="scripts/about-clients.php" method="post">
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Client</h3>
            </div>
			
              <div class="box-body">
				
				<div class="form-group col-md-6">
                  <label for="Content" class="col-sm-12 control-label">Picture (Picture otomatis di resize ke 291x414px. kosongkan jika tidak ingin diganti)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				<div class="form-group col-md-6">
					<label for="part_status" class="col-sm-12 control-label">Status</label>
					<div class="col-sm-12">
						<select class="form-control" name="part_status">
							<option value="Active" <?php if($status == "Active") echo "selected" ?>>Active</option>
							<option value="Inactive" <?php if($status == "Inactive") echo "selected" ?>>Inactive</option>
						</select>
					</div>
                </div>
				
              </div>
              
			  <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="about-clients.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /Modifikasi Clients -->
		
      </div>
    </section>
  </div>

  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>

<script>
$(function () {
	$('#example1').DataTable();
});
</script>
</body>
</html>