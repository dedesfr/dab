<?php
session_start();
include_once "base/koneksi.php";

$idadmin = @$_SESSION['idadmin'];
if($idadmin != ""){
header("location:dashboard.php");
}
$err = "";
$err = @$_SESSION['error'];
/*
require_once __DIR__ . '/src/autoload.php';
$siteKey = '6LdeVxAUAAAAAJtKL2NopanvfzhbfPYn3_NkpeUg';
$secret = '6LdeVxAUAAAAADr0Lqr61rwfTPzRNSwfPMto02Mq';
$lang = 'en';
*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>DAB</b>ADM</a>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg"><?php if($err != "") echo $err; else echo "Sign in to start your session"; ?></p>

    <form action="scripts/login.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="uname">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="pass">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	  <!--<div class="form-group has-feedback">
        <div class="g-recaptcha" data-sitekey="6LdeVxAUAAAAAJtKL2NopanvfzhbfPYn3_NkpeUg"></div>
      </div>-->
      <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
	
  </div>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>