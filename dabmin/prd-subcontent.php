<?php
session_start();
include("base/koneksi.php");
$page		= "prd";
$pagetree	= "prdsubcontent";

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}

$id = "";
$id = @$_GET['id'];
$namaOpr = $_SESSION['nama'];
$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <script src="http://cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.html"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Sub-products<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Daftar product -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Sub-product</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>product</th>
				  <th>Sub-product</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$prdQ = mysqli_query($con, "select * FROM ms_products s, ms_subproducts ss WHERE s.prd_id = ss.prd_id ORDER BY ss.prd_id ASC");
				while($prd = mysqli_fetch_array($prdQ)){
				?>
				<tr>
				  <td><a href="prd-subcontent.php?id=<?php echo $prd['subprd_id']; ?>">Edit</a></td>
                  <td><?php echo $prd['prd_name']; ?></td>
                  <td><?php echo $prd['subprd_name']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Daftar product -->
		
        <!-- Modifikasi product -->
		<?php
		if($id != ""){
		$editPrdCntQ = mysqli_query($con, "select * FROM ms_products s, ms_subproducts ss WHERE s.prd_id = ss.prd_id AND ss.subprd_id = $id");
		$editPrdCnt = mysqli_fetch_array($editPrdCntQ);
    $editPrdCnt['subprd_content'] = str_replace("gambar/prdSubcontent/","../gambar/prdSubcontent/",$editPrdCnt['subprd_content']);
		?>
		<form enctype="multipart/form-data" action="scripts/products-subcontent.php" method="post">
		<input type="hidden" value="<?php echo $id; ?>" name="idsprd" />
		<div class="col-md-12">

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit product</h3>
            </div>
			
              <div class="box-body">
			  
				<div class="form-group col-md-6">
                  <label for="prd_name" class="col-sm-12 control-label">Nama product</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="prd_name" value="<?php echo $editPrdCnt['prd_name']; ?>" readonly>
                  </div>
                </div>
				
				<div class="form-group col-md-6">
                  <label for="subprd_name" class="col-sm-12 control-label">Nama Sub-product</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="subprd_name" value="<?php echo $editPrdCnt['subprd_name']; ?>" readonly>
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="picture" class="col-sm-12 control-label">Picture (Picture otomatis di resize ke 770x435px)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="subprd_content" class="col-sm-12 control-label">Long Description (untuk Halaman products)</label>
                  <div class="col-sm-12">
                    <textarea name="subprd_content" id="editor2" rows="10" cols="80"><?php echo $editPrdCnt['subprd_content']; ?></textarea>
                  </div>
                </div>	

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-subcontent.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /Modifikasi product -->
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script>
$(function () {
	$('#example1').DataTable();
	<?php if($id != "") { ?>
	var wordCountConf1 = {
		showParagraphs: false,
		showWordCount: false,
		showCharCount: true,
		countSpacesAsChars: true,
		countHTML: true
	}
	
	// CKEDITOR.replace('editor1', {wordcount: wordCountConf1});
	CKEDITOR.replace( 'editor2', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload.php",
	} );
	<?php } ?>
});
</script>
</body>
</html>