<?php
session_start();
include("base/koneksi.php");
$page		= "prd";
$pagetree	= "prdcategory";

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}

$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$idprd = "";
$idprd = @$_GET['idprd'];

$idsprd = "";
$idsprd = @$_GET['idsprd'];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/css/font-awesome-4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.html"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>products<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
		
		<!-- add product -->
		<?php if($idprd == "" && $idsprd == ""){ ?>
		<form enctype="multipart/form-data" action="scripts/products.php" method="post">
        <input type="hidden" value="product" name="prd" />
		<input type="hidden" value="x" name="idprd" />
		<div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah product</h3>
            </div>
              <div class="box-body">
								<div class="form-group col-md-6">
									<label for="prd_id" class="col-sm-12 control-label">product</label>
									<div class="col-sm-12">
										<select class="form-control" name="prd_type">
											<option value="0">Product</option>
											<option value="1">Accessories</option>
										</select>
									</div>
								</div>
                <div class="form-group col-md-6">
                  <label for="prd_name" class="col-sm-12 control-label">Nama product</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="prd_name">
                  </div>
                </div>

								<div class="form-group col-md-6">
									<label for="picture" class="col-sm-12 control-label">Picture</label>
									<div class="col-sm-12">
										<input type="file" name="file" id="file" /> 
									</div>
								</div>

              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-category.php" class="btn btn-default">Reset</a>
			  </div>
          </div>
        </div>
        </form>
		
		<form action="scripts/products.php" method="post">
        <input type="hidden" value="subproduct" name="prd" />
		<input type="hidden" value="x" name="idsprd" />
		<div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Sub-product</h3>
            </div>
              <div class="box-body">
			  
				<div class="form-group col-md-6">
					<label for="prd_id" class="col-sm-12 control-label">product</label>
					<div class="col-sm-12">
						<select class="form-control" name="prd_id">
							<option value="">Pilih product..</option>
							<?php
							$pickPrdQ = mysqli_query($con, "select prd_id, prd_name FROM ms_products ORDER BY prd_id ASC");
							while($pickPrd = mysqli_fetch_array($pickPrdQ)){
							?>
							<option value="<?php echo $pickPrd['prd_id']; ?>"><?php echo $pickPrd['prd_name']; ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
                <div class="form-group col-md-6">
                  <label for="subprd_name" class="col-sm-12 control-label">Nama Sub-product</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="subprd_name">
                  </div>
                </div>

              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-category.php" class="btn btn-default">Reset</a>
			  </div>
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /add product -->
		
        <!-- Modifikasi product -->
		<?php
		if($idprd != ""){
		$editPrdQ = mysqli_query($con, "select * FROM ms_products WHERE prd_id = '$idprd'");
		$editPrd = mysqli_fetch_array($editPrdQ);
		?>
		<form enctype="multipart/form-data" action="scripts/products.php" method="post">
        <input type="hidden" value="product" name="prd" />
		<input type="hidden" value="<?php echo $idprd; ?>" name="idprd" />
		<div class="col-md-12">
          <div class="box box-info">
		  
            <div class="box-header with-border">
              <h3 class="box-title">Edit product</h3>
            </div>
			
				<div class="box-body">
			  
					<div class="form-group col-md-4">
					  <label for="prd_name" class="col-sm-12 control-label">Nama product</label>
					  <div class="col-sm-12">
						<input type="text" class="form-control" name="prd_name" value="<?php echo $editPrd['prd_name']; ?>">
					  </div>
					</div>
					<div class="form-group col-md-4">
						<label for="prd_id" class="col-sm-12 control-label">product</label>
						<div class="col-sm-12">
							<select class="form-control" name="prd_type">
							<option value="1" <?php if($editPrd['prd_type'] == 1) echo "selected" ?>>Accessories</option>
							<option value="0" <?php if($editPrd['prd_type'] == 0) echo "selected" ?>>Product</option>
							</select>
						</div>
					</div>
					<div class="form-group col-md-4">
						<label for="picture" class="col-sm-12 control-label">Picture (Picture otomatis di resize ke 300x300px)</label>
						<div class="col-sm-12">
							<input type="file" name="file" id="file" /> 
						</div>
					</div>

				</div>
				
				<div class="box-footer" style="background:#eee">
					<button type="submit" class="btn btn-primary pull-right">Submit</button>
					<a href="prd-category.php" class="btn btn-default">Reset</a>
				</div>
          </div>
        </div>
        </form>
		<?php } ?>
		
		<?php
		if($idsprd != ""){
		$editSPrdQ = mysqli_query($con, "select * FROM ms_subproducts WHERE subprd_id = '$idsprd'");
		$editSPrd = mysqli_fetch_array($editSPrdQ);
		?>
		<form action="scripts/products.php" method="post">
        <input type="hidden" value="subproduct" name="prd" />
		<input type="hidden" value="<?php echo $idsprd; ?>" name="idsprd" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Sub-product</h3>
            </div>
              <div class="box-body">
			  
				<div class="form-group col-md-6">
					<label for="prd_id" class="col-sm-12 control-label">product</label>
					<div class="col-sm-12">
						<select class="form-control" name="prd_id">
							<option value="">Pilih product..</option>
							<?php
							$pickPrdQ = mysqli_query($con, "select prd_id, prd_name FROM ms_products ORDER BY prd_id ASC");
							while($pickPrd = mysqli_fetch_array($pickPrdQ)){
							?>
							<option value="<?php echo $pickPrd['prd_id']; ?>" <?php if($pickPrd['prd_id'] == $editSPrd['prd_id']) echo "selected"; ?>><?php echo $pickPrd['prd_name']; ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
				<div class="form-group col-md-6">
                  <label for="subprd_name" class="col-sm-12 control-label">Nama Sub-product</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="subprd_name" value="<?php echo $editSPrd['subprd_name']; ?>">
                  </div>
                </div>
				
			  </div>
			  
			  <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="prd-category.php" class="btn btn-default">Reset</a>
			  </div>
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /Modifikasi product -->
		
		<div class="clearfix"></div>
		
		<!-- products -->
		<div class="col-md-5">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">product List</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>product</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$prdQ = mysqli_query($con, "SELECT prd_name, prd_id FROM ms_products ORDER BY prd_id ASC");
				while($prd = mysqli_fetch_array($prdQ)){
				?>
				<tr>
				  <td><a href="prd-category.php?idprd=<?php echo $prd['prd_id']; ?>">Edit</a> | <a href="scripts/products.php?prd=delprd&idprd=<?php echo $prd['prd_id']; ?>">Delete</a></td>
                  <td><?php echo $prd['prd_name']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /products -->
		
		<!-- Sub products -->
		<div class="col-md-7">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sub-product List</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>product</th>
				  <th>Sub-product</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$sprdQ = mysqli_query($con, "SELECT ss.subprd_id, s.prd_name, ss.subprd_name FROM ms_products s, ms_subproducts ss WHERE s.prd_id = ss.prd_id ORDER BY s.prd_id ASC");
				while($sprd = mysqli_fetch_array($sprdQ)){
				?>
				<tr>
				  <td><a href="prd-category.php?idsprd=<?php echo $sprd['subprd_id']; ?>">Edit</a> | <a href="scripts/products.php?prd=delsprd&idsprd=<?php echo $sprd['subprd_id']; ?>">Delete</a></td>
                  <td><?php echo $sprd['prd_name']; ?></td>
                  <td><?php echo $sprd['subprd_name']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Sub products -->
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script>
$(function () {
	$('#example1').DataTable();
	$('#example2').DataTable();
});
</script>
</body>
</html>