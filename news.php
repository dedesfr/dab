<?php
include("base/koneksi.php");
$page = "news";

$id = "";
$id = @$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "base/head.php"; ?>
</head>
<body class="body-left">

<div class="loading">
  <div class="table">
    <div class="inner"> <img src="images/logo.png" alt="Image" class="logo"> </div>
  </div>
</div>

<div class="transition-overlay"></div>
<?php include "base/header.php"; ?>

<section class="internal-header overlay-dark" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="title">Newsroom</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">Newsroom</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- end internal-header -->

<?php if($id == ""){ ?>
<section class="blog">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
		<?php
		$result = mysqli_query($con, "SELECT COUNT(*) FROM tr_news");
		$r = mysqli_fetch_row($result);
		$numrows = $r[0];

		$rowsperpage = 2;
		$totalpages = ceil($numrows / $rowsperpage);

		if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage']))
			$currentpage = (int) $_GET['currentpage'];
		else
			$currentpage = 1;

		if ($currentpage > $totalpages)
		   $currentpage = $totalpages;
	   
		if ($currentpage < 1) 
			$currentpage = 1;

		$offset = ($currentpage - 1) * $rowsperpage;
		$result = mysqli_query($con, "SELECT * FROM tr_news ORDER BY n_date DESC LIMIT $offset, $rowsperpage");
		while ($query = mysqli_fetch_assoc($result)) {
		$datee = new dateTime($query['n_date']);
		$datee = $datee->format('F j, Y');
		$headline = strpos($query['n_content'], ' ', 140);
		$headline2 = substr($query['n_content'],0,$headline);
		$headline2 = substr($headline2, 3);
		?>
        <div class="news-box">
          <figure><img src="<?php echo $query['n_picture']; ?>" alt="<?php echo $query['n_header']; ?>"> <span class="date"><?php echo $datee; ?></span> </figure>
          <h3><?php echo $query['n_header']; ?></h3>
          <p><?php echo $headline2."..."; ?></p>
        <a href="news.php?id=<?php echo $query['n_id']; ?>" class="read-link">Read more</a>
        </div>
		<?php } ?>
      </div>
      <!-- end col-8 -->
      <div class="col-md-4">
        <aside class="blog-sidebar">
			<div class="widgets social-media">
				<h2>DAB Pumps Indonesia</h2>
				<p>Produk Pompa air DAB dipercaya karena kualitas , disukai karena handal, dipilih karena menguntungkan, begitu jargon dari DAB yang sering kita dengar dan ternyata tidak berlebihan juga karena DAB sudah membuktikannya kepada pemakai poma air DAB di Indonesia dan beberapa pengakuan / sertifikat dunia yang diperolehnya seperti, IQ Net, ESQ dan SNI tentunya.</p>
			</div>
        </aside>
      </div>
      <!-- end col-4 -->
      
      <div class="col-xs-12">
		<ul class="pagination">
			<?php
			$range = 2;
			if ($currentpage > 1) 
			{
			   $prevpage = $currentpage - 1; ?>
			   <li><a href="news.php?currentpage=<?php echo $prevpage; ?>" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a></li>
			   <li class="active"><a href="#"><?php echo $currentpage; ?><span class="sr-only">(current)</span></a></li>
			   <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>
			<?php 
			}
			if ($currentpage != $totalpages) 
			{
				$nextpage = $currentpage + 1; ?>
				<li><a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a></li>
				<li class="active"><a href="#"><?php echo $currentpage; ?><span class="sr-only">(current)</span></a></li>
				<li><a href="news.php?currentpage=<?php echo $nextpage; ?>" aria-label="Next"> <span aria-hidden="true">&raquo;</span></a></li>
			<?php } ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<!-- end news list -->

<!-- news detail -->
<?php if($id != ""){ 
$query = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_news WHERE n_id = $id"));
$datee = new dateTime($query['n_date']);
$datee = $datee->format('F j, Y');
?>
<section class="blog">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="news-box">
          <figure><img src="<?php echo $query['n_picture']; ?>"> <span class="date"><?php echo $datee; ?></span> </figure>
          <h3><?php echo $query['n_header']; ?></h3>
			<?php echo $query['n_content']; ?>
        </div>
      </div>
      <div class="col-md-4">
        <aside class="blog-sidebar">
			<div class="widgets social-media">
				<h2>DAB Pumps Indonesia</h2>
				<p>Produk Pompa air DAB dipercaya karena kualitas , disukai karena handal , dipilih karena menguntungkan , begitu jargon dari DAB yang sering kita dengar dan ternyata tidak berlebihan juga karena DAB sudah membuktikannya kepada pemakai poma air DAB di Indonesia dan beberapa pengakuan / sertifikat dunia yang diperolehnya seperti, IQ Net, ESQ dan SNI tentunya.</p>
			</div>
			
			<div class="widgets social-media">
				<h2>Recent Articles</h2>
				
				<?php
				$result = mysqli_query($con, "SELECT * FROM tr_news ORDER BY n_date DESC LIMIT 2");
				while ($query = mysqli_fetch_assoc($result)) {
				$datee = new dateTime($query['n_date']);
				$datee = $datee->format('F j, Y');
				$headline = strpos($query['n_header'], ' ', 20);
				$headline2 = substr($query['n_header'],0,$headline);
				?>
				<figure><a href="news.php?id=<?php echo $query['n_id']; ?>">
					<img src="<?php echo $query['n_picture']; ?>"> <span class="date"><?php echo $datee; ?></span> 
				</a></figure>
				<h4><a href="news.php?id=<?php echo $query['n_id']; ?>"><?php echo $query['n_header']; ?></a></h4>
				<br />
				<?php } ?>
				
			</div>
			
        </aside>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<!-- end news detail -->

<section class="newsletter">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7 col-xs-12">
        <div class="titles">
          <h6>SIGN UP FOR OUR </h6>
          <h2>NEWSLETTER</h2>
        </div>
        <form>
          <div class="form-group">
            <input type="text" placeholder="Your e-mail">
            <button type="submit">SUBSCRIBE</button>
          </div>
          <small>I promise, we won’t spam you!</small>
        </form>
      </div>
      <div class="col-md-4 col-sm-5 hidden-xs"></div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include "base/footer.php"; ?>

<a href="#0" class="cd-top"></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
(function($) {
  $(window).load(function(){
    $(".loading").addClass("fade-out");
    $(".loading .table .inner").addClass("fade-out-inner");
    
  });
})(jQuery)
</script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-slider.js"></script> <script src="js/jquery.stellar.js"></script> 
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script> 
<script src="js/isotope.min.js"></script> 
<script src="js/masonry.js"></script> 
<script src="js/owl.carousel.js"></script> 
<script src="js/wow.js"></script> 
<script src="js/counter.js"></script> 
<script src='js/jquery.themepunch.tools.min.js' type='text/javascript' ></script> <script src='js/jquery.themepunch.revolution.min.js' type='text/javascript' ></script> 
<script src='js/jquery.validate.min.js' type='text/javascript' ></script> 
<script src='js/jquery.form.js' type='text/javascript' ></script> 
<script src='js/contact-form.js' type='text/javascript' ></script> 
<script src='js/settings.js' type='text/javascript' ></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.counterup.min.js"></script>
<script src="js/scripts.js"></script> 
</body>
</html>