<?php
include("base/koneksi.php");
$page = "about";

$about = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM ms_text WHERE txt_id = 1"));
?><!DOCTYPE html>
<html lang="en">
<head>
<?php include "base/head.php"; ?>
</head>
<body class="body-left">

<div class="loading">
  <div class="table">
    <div class="inner"> <img src="images/logo.png" alt="Image" class="logo"> </div>
  </div>
</div>

<div class="transition-overlay"></div>
<?php include "base/header.php"; ?>

<section class="internal-header overlay-dark" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="title">ABOUT US</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">About Us</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- end internal-header -->

<section class="text-content">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
		  <h2><?php echo $about['txt_about_judul']; ?></h2>
		  <?php echo $about['txt_about_content']; ?>
      </div>
	  <div class="col-md-4"> 
		<figure><img src="<?php echo $about['txt_about_picture']; ?>" title="DAB Pumps Indonesia" /> </figure>
	  </div>
    </div>
  </div>
</section>
<!-- end text-content -->

<section class="team-members">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p style="margin-left:.5in; margin-right:44.15pt"><em><span style="font-size:15.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#709472">&ldquo;Simple and efficient solutions are the biggest types of </span></span></span></em><em><span style="font-size:15.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#709472">innovations. The </span></span></span></em><em><span style="font-size:15.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#709472">technology of our products</span></span></span></em></p>

<p style="margin-left:.5in; margin-right:97.35pt"><em><span style="font-size:15.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#709472">speaks the same language of those who buy or use them. This is our </span></span></span></em><em><span style="font-size:15.0pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#709472">strength.&rdquo;</span></span></span></em></p>
      </div>
    </div>
  </div>
</section>
<!-- end team-members -->

<section class="team-members">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
		<h2>Our Certificates</h2>
        <?php
		$certQ = mysqli_query($con, "SELECT * FROM tr_clients ORDER BY part_id ASC");
		while($cert = mysqli_fetch_array($certQ)){ ?>
		<div class="col-md-4">
			<figure><img src="<?php echo $cert['part_picture']; ?>"></figure>
		</div>
		<?php } ?> 
      </div>
    </div>
  </div>
</section>
<!--Stats-->

<section class="quote background1 overlay-yellow" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h5>MORE INFORMATION FILL UP TO FORM</h5>
        <h2>LET'S GET OFFER !</h2>
        <a href="contact-us.php">GET QUOTE <i class="ion-chevron-right"></i></a> </div>
    </div>
  </div>
</section>
<!-- end quote -->

<!-- Footer -->
<?php include "base/footer.php"; ?>

<a href="#0" class="cd-top"></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
(function($) {
	$(window).load(function(){
		$(".loading").addClass("fade-out");
		$(".loading .table .inner").addClass("fade-out-inner");
		
	});
})(jQuery)
</script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-slider.js"></script> <script src="js/jquery.stellar.js"></script> 
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script> 
<script src="js/isotope.min.js"></script> 
<script src="js/masonry.js"></script> 
<script src="js/owl.carousel.js"></script> 
<script src="js/wow.js"></script> 
<script src="js/counter.js"></script> 
<script src='js/jquery.themepunch.tools.min.js' type='text/javascript' ></script> <script src='js/jquery.themepunch.revolution.min.js' type='text/javascript' ></script> 
<script src='js/jquery.validate.min.js' type='text/javascript' ></script> 
<script src='js/jquery.form.js' type='text/javascript' ></script> 
<script src='js/contact-form.js' type='text/javascript' ></script> 
<script src='js/settings.js' type='text/javascript' ></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.counterup.min.js"></script>
<script src="js/scripts.js"></script> 
</body>
</html>