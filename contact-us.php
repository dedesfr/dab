<?php
include("base/koneksi.php");
$page = "contact";

$id = "";
$id = @$_GET['id'];

$query = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM ms_text WHERE txt_id = 1"));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "base/head.php"; ?>
</head>
<body class="body-left">
<div class="loading">
  <div class="table">
    <div class="inner"> <img src="images/logo.png" alt="Image" class="logo"> </div>
    <!-- end inner --> 
  </div>
  <!-- end table --> 
</div>
<!-- end loading -->
<div class="transition-overlay"></div>
<?php include "base/header.php"; ?>

<section class="internal-header overlay-dark" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="title">CONTACT US</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">Contact Us</li>
        </ol>
      </div>
      <!-- end col-12 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end internal-header -->
<section class="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="titles">
          <h6>CONTACT AND ADDRESS</h6>
          <h2>INFORMATION</h2>
        </div>
      </div>
      
      <div class="col-md-8 col-xs-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9894680115335!2d106.87452991473211!3d-6.2651145954652385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f2fad3e96e45%3A0x8d103416fd5fbcc9!2sIntirub+(Grha+Intirub+Business+Park+-+Mega+Manunggal+Property)!5e0!3m2!1sen!2sid!4v1499247947625" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="col-md-4 col-xs-12">
        <h4><?php echo $query['txt_contact_name']; ?></h4>
        <br>
        <address>
        <?php echo $query['txt_contact_content']; ?>
        </address>
      </div>
	  
      <div class="col-xs-12">
        <div class="titles">
          <h6>CONTACT FORM TO</h6>
          <h2>REACH US</h2>
        </div>
      </div>
      <div class="col-xs-12">
        <form class="contact-form" id="contact" name="contact" method="post">
          <div class="form-group">
            <label>Your name</label>
            <input type="text" name="name" id="name" required>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <label>E-mail</label>
            <input type="text" name="email" id="email" required>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" id="phone" required>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <label>City</label>
            <input type="text" name="city" id="city" required>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <label>About what ?</label>
            <input type="text" name="subject" id="subject" required>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <label>Your Message</label>
            <textarea name="message" id="message" required></textarea>
          </div>
          <!-- end form-group -->
          <div class="form-group">
            <button id="submit" type="submit" name="submit">SEND IT</button>
          </div>
          <!-- end form-group -->
        </form>
      </div>
      <div class="col-xs-12">
      	<div id="success" class="alert alert-success" role="alert">
          <p>Your message was sent successfully! We will be in touch as soon as we can.</p>
        </div>
        <div id="error" class="alert alert-danger" role="alert">
          <p>Something went wrong, try refreshing and submitting the form again.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end gallery -->

<section class="newsletter">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7 col-xs-12">
        <div class="titles">
          <h6>SIGN UP FOR OUR </h6>
          <h2>NEWSLETTER</h2>
        </div>
        <!-- end title -->
        <form>
          <div class="form-group">
            <input type="text" placeholder="Your e-mail">
            <button type="submit">SUBSCRIBE</button>
          </div>
          <small>I promise, we won’t spam you!</small>
        </form>
      </div>
      <!-- end col-8 -->
      <div class="col-md-4 col-sm-5 hidden-xs"></div>
      <!-- end col-4 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end newsletter -->

<!-- Footer -->
<?php include "base/footer.php"; ?>

<a href="#0" class="cd-top"></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
(function($) {
	$(window).load(function(){
		$(".loading").addClass("fade-out");
		$(".loading .table .inner").addClass("fade-out-inner");
		
	});
})(jQuery)
</script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-slider.js"></script> <script src="js/jquery.stellar.js"></script> 
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script> 
<script src="js/isotope.min.js"></script> 
<script src="js/masonry.js"></script> 
<script src="js/owl.carousel.js"></script> 
<script src="js/wow.js"></script> 
<script src="js/counter.js"></script> 
<script src='js/jquery.themepunch.tools.min.js' type='text/javascript' ></script> <script src='js/jquery.themepunch.revolution.min.js' type='text/javascript' ></script> 
<script src='js/jquery.validate.min.js' type='text/javascript' ></script> 
<script src='js/jquery.form.js' type='text/javascript' ></script> 
<script src='js/contact-form.js' type='text/javascript' ></script> 
<script src='js/settings.js' type='text/javascript' ></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.counterup.min.js"></script> 
<script src="js/scripts.js"></script> 
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script src="js/google-maps.js" type="text/javascript"></script>
</body>
</html>