<?php
include("base/koneksi.php");
$page = "home";

$id = "";
$id = @$_GET['id'];
$home_content = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM ms_home_content WHERE hc_id = 1"));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "base/head.php"; ?>
</head>
<body class="body-left">

<div class="loading">
  <div class="table">
    <div class="inner"> <img src="images/logo.png" alt="Image" class="logo"> </div>
  </div>
</div>

<div class="transition-overlay"></div>
<?php include "base/header.php"; ?>

<section class="slider">
  <div class="banner">
    <div class="slider-content">
      <div id="rev_slider_35_1" class="rev_slider fullwidthabanner">
        <ul>
		<?php
		$sliQ = mysqli_query($con, "SELECT * FROM ms_slider ORDER BY sli_id DESC");
		while($sli = mysqli_fetch_array($sliQ)){ ?>
          <li data-transition="zoomout" data-masterspeed="700"> <img src="<?php echo $sli['sli_image']; ?>" alt="<?php echo $sli['sli_text1']." ".$sli['sli_text2']; ?>">
            <div class="tp-caption lfb ltt tp-resizeme start" 
                data-x="center" 
                data-hoffset="0" 
                data-y="center" 
                data-voffset="-20" 
                data-speed="600" 
                data-start="200" 
                data-easing="Power4.easeOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="500" 
                data-endeasing="Power4.easeIn">
              <h2> <?php echo $sli['sli_text1']; ?></h2>
            </div>
            <div class="tp-caption lfb ltt tp-resizeme start" 
                data-x="center" 
                data-hoffset="0" 
                data-y="center" 
                data-voffset="40" 
                data-speed="600" 
                data-start="300" 
                data-easing="Power4.easeOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="500" 
                data-endeasing="Power4.easeIn">
              <h6> <?php echo $sli['sli_text2']; ?></h6>
            </div>
          </li>
        <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- end slider -->

<section class="home-services">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="titles">
          <h6>Water Technology</h6>
          <h2>DAB Pumps Product</h2>
        </div> 
      </div>
	<?php
	$prdQ = mysqli_query($con, "SELECT * FROM ms_products");
	while($prd = mysqli_fetch_array($prdQ)){ ?>
      <div class="col-md-3 col-sm-6">
        <a href="product.php?id=<?php echo $prd['prd_id']; ?>"><figure><img width="50%" src="<?php echo $prd['prd_picture']; ?>"></figure></a>
        <h4 style="height:240px"><?php echo $prd['prd_name']; ?></h4>
      </div>
    <?php } ?>
    </div>
  </div>
</section>
<!-- end products -->

<section class="quote background1 overlay-yellow" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h5>MORE INFORMATION TO OUR WIDE-RANGE PRODUCTS</h5>
        <h2>SEE MORE</h2>
        <p>Kami menyediakan produk orisinil DAB Pumps yang siap diantar kapan saja !</p>
        <a href="product.php">PRODUCT LIST<i class="ion-chevron-right"></i></a> </div>
    </div>
  </div>
</section>
<!-- end quote -->

<section class="about-us">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="titles">
          <h6><?php echo $home_content['judul']; ?></h6>
          <h2><?php echo $home_content['sub_judul']; ?></h2>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <div class="left-image">
          <figure><img src="gambar/about/about.png" alt="Image"> </figure>
        </div>
      </div>
      <div class="col-md-8 col-sm-12">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="services-box">
              <?php echo $home_content['content']; ?>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end about-us -->

<!-- footer -->
<?php include "base/footer.php"; ?>

<a href="#0" class="cd-top"></a> 

<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
(function($) {
	$(window).load(function(){
		$(".loading").addClass("fade-out");
		$(".loading .table .inner").addClass("fade-out-inner");
		
	});
})(jQuery)
</script>
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-slider.js"></script> 
<script src="js/jquery.stellar.js"></script> 
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script> 
<script src="js/isotope.min.js"></script> 
<script src="js/masonry.js"></script> 
<script src="js/owl.carousel.js"></script> 
<script src="js/wow.js"></script> 
<script src="js/counter.js"></script> 
<script src='js/jquery.themepunch.tools.min.js' type='text/javascript' ></script> 
<script src='js/jquery.themepunch.revolution.min.js' type='text/javascript' ></script> 
<script src='js/jquery.validate.min.js' type='text/javascript' ></script> 
<script src='js/jquery.form.js' type='text/javascript' ></script> 
<script src='js/contact-form.js' type='text/javascript' ></script> 
<script src='js/settings.js' type='text/javascript' ></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.counterup.min.js"></script>
<script src="js/scripts.js"></script>  
</body>
</html>