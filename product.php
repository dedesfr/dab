<?php
include("base/koneksi.php");
$page = "product";

$id = "";
$id = @$_GET['id'];
$subid = "";
$subid = @$_GET['subid'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "base/head.php"; ?>
</head>
<body class="body-left">
<div class="loading">
  <div class="table">
    <div class="inner"> <img src="images/logo.png" alt="Image" class="logo"> </div>
  </div>
</div>
<div class="transition-overlay"></div>
<?php include "base/header.php"; ?>

<section class="internal-header overlay-dark" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="title">PRODUCTS</h2>
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">Product</li>
        </ol>
      </div>
      <!-- end col-12 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end internal-header -->

<?php if($id == "" && $subid == ""){ ?>
<section class="all-projects">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="titles">
          <h2>DAB Pumps</h2>
        </div>
      </div>
    </div>
    
	<div class="row">
		<?php
		$queryQ = mysqli_query($con, "SELECT * FROM ms_products WHERE prd_type = 0 ORDER BY prd_id ASC");
		while($query = mysqli_fetch_array($queryQ)){ ?>
		  <div class="col-md-4 col-sm-4 col-xs-12">
			<div class="product-box">
			  <figure>
				  <a href="product.php?id=<?php echo $query['prd_id']; ?>"><img src="<?php echo $query['prd_picture']; ?>" alt="<?php echo $query['prd_name']; ?>"></a>
				</figure>
				<h5 style="height:30px"><?php echo $query['prd_name']; ?></h5>
			</div>
			<!-- end product-box -->
		  </div>
		<?php } ?>
	</div>
	
  </div>
</section>
<!-- end Products -->

<!-- Accessories -->
<section class="all-projects">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="titles">
          <h2>Pump Accessories</h2>
        </div>
      </div>
    </div>
    
	<div class="row">
		<?php
		$queryQ = mysqli_query($con, "SELECT * FROM ms_products WHERE prd_type = 1 ORDER BY prd_id ASC");
		while($query = mysqli_fetch_array($queryQ)){ ?>
		  <div class="col-md-4 col-sm-4 col-xs-12">
			<div class="product-box">
			  <figure>
				  <a href="product.php?id=<?php echo $query['prd_id']; ?>"><img src="<?php echo $query['prd_picture']; ?>" alt="<?php echo $query['prd_name']; ?>"></a>
				</figure>
				<h5 style="height:30px"><?php echo $query['prd_name']; ?></h5>
			</div>
			<!-- end product-box -->
		  </div>
		<?php } ?>
	</div>
	
  </div>
</section>
<!-- /Accessories -->
<?php } ?>

<!-- product detail -->
<?php if($id != "" || $subid != ""){ 
if($subid == "")
$query = mysqli_fetch_array(mysqli_query($con, "SELECT prd.prd_name, sub.* 
FROM ms_subproducts sub, ms_products prd
WHERE sub.prd_id = $id AND sub.prd_id = prd.prd_id
ORDER BY sub.subprd_id DESC LIMIT 1"));
else
$query = mysqli_fetch_array(mysqli_query($con, "SELECT prd.prd_name, sub.* 
FROM ms_subproducts sub, ms_products prd
WHERE sub.subprd_id = $subid AND sub.prd_id = prd.prd_id"));

$idSub = $query['subprd_id'];
?>
<section class="text-content">
  <div class="container">
    <div class="row">
      <section class="shop">
	  
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="item-detail-left">
            <div class="item-images">
              <figure class="main-image"><img src="<?php echo $query['subprd_picture']; ?>" alt="<?php echo $query['subprd_name']; ?>" title="<?php echo $query['subprd_name']; ?>"></figure>
            </div>
          </div>
        </div>
		
        <!-- variants -->
        <div class="col-md-4 col-sm-12 col-xs-12">
          <aside class="content-side-menu">
			<h3><?php echo $query['prd_name']; ?></h3>
            <ul>
				<?php $varQ = mysqli_query($con, "SELECT subprd_id, subprd_name FROM ms_subproducts WHERE prd_id = $id ORDER BY subprd_id ASC");
				while($var = mysqli_fetch_array($varQ)){ ?>
                <li <?php if($idSub == $var['subprd_id']) echo 'class="active"'; ?>><a class="transition" href="product.php?id=<?php echo $id; ?>&subid=<?php echo $var['subprd_id']; ?>"><?php echo $var['subprd_name']; ?></a></li>
				<?php } ?>
              </ul>
          </aside>
        </div>
        <!-- variants -->
		
      </section>
	  
	  <div class="col-md-12">
        <p class="lead"><?php echo $query['subprd_name']; ?></p>
        <?php echo $query['subprd_content']; ?>
        
		<br />
		<br />
		<div class="clearfix"></div>
		<br />
		<br />
      
		
		<?php
		$pdfQ = mysqli_query($con, "SELECT * FROM tr_prd_document WHERE subprd_id = $idSub");
		if(mysqli_num_rows($pdfQ) != 0){		
		?>		
		<div class="brochure">
			<div class="title-box"> Product Brochures </div>
            <div class="content"> <img src="images/icon7.png" alt="Image" class="icon"></div>
        </div>
        <ul class="custom-list">
        
		<?php while($pdf = mysqli_fetch_array($pdfQ)){ ?>
		  <li>
            <i class="ion-checkmark"></i> 
              <a href="<?php echo $pdf['doc_file']; ?>" download><?php echo $pdf['doc_name']; ?></a>
          </li>
		<?php } ?>
		
        </ul>
      </div>
	  <?php } ?>
	  
    </div>
  </div>
</section>

<section class="all-projects">
  <div class="container">
	
	<div class="row">
      <div class="col-md-12">
        <div class="titles">
		  <h6>ALL LISTING RELATED</h6>
          <h2>DAB Pumps</h2>
        </div>
      </div>
    </div>
    
	<div class="row">
		<?php
		$queryQ = mysqli_query($con, "SELECT * FROM ms_products WHERE prd_type = 0 AND prd_id != $id ORDER BY prd_id ASC");
		while($query = mysqli_fetch_array($queryQ)){ ?>
		  <div class="col-md-3 col-sm-4 col-xs-12">
			<div class="product-box">
			  <figure>
				  <a href="product.php?id=<?php echo $query['prd_id']; ?>"><img src="<?php echo $query['prd_picture']; ?>" alt="<?php echo $query['prd_name']; ?>"></a>
				</figure>
				<h5 style="height:30px"><?php echo $query['prd_name']; ?></h5>
			</div>
			<!-- end product-box -->
		  </div>
		<?php } ?>
	</div>
	
  </div>
</section>
<!-- end Products -->

<!-- Accessories -->
<section class="all-projects">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="titles">
          <h2>Pump Accessories</h2>
        </div>
      </div>
    </div>
    
	<div class="row">
		<?php
		$queryQ = mysqli_query($con, "SELECT * FROM ms_products WHERE prd_type = 1 AND prd_id != $id ORDER BY prd_id ASC");
		while($query = mysqli_fetch_array($queryQ)){ ?>
		  <div class="col-md-3 col-sm-4 col-xs-12">
			<div class="product-box">
			  <figure>
				  <a href="product.php?id=<?php echo $query['prd_id']; ?>"><img src="<?php echo $query['prd_picture']; ?>" alt="<?php echo $query['prd_name']; ?>"></a>
				</figure>
				<h5 style="height:30px"><?php echo $query['prd_name']; ?></h5>
			</div>
			<!-- end product-box -->
		  </div>
		<?php } ?>
	</div>
	
  </div>
</section>
<!-- /Accessories -->
<?php } ?>

<section class="newsletter">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7 col-xs-12">
        <div class="titles">
          <h6>SIGN UP FOR OUR </h6>
          <h2>NEWSLETTER</h2>
        </div>
        <!-- end title -->
        <form>
          <div class="form-group">
            <input type="text" placeholder="Your e-mail">
            <button type="submit">SUBSCRIBE</button>
          </div>
          <small>I promise, we won’t spam you!</small>
        </form>
      </div>
      <!-- end col-8 -->
      <div class="col-md-4 col-sm-5 hidden-xs"></div>
      <!-- end col-4 --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end newsletter -->

<!-- footer -->
<?php include "base/footer.php"; ?>

<a href="#" class="cd-top"></a> 

<!-- JS FILES --> 
<script src="js/jquery.min.js"></script> 
<script type="text/javascript">
(function($) {
	$(window).load(function(){
		$(".loading").addClass("fade-out");
		$(".loading .table .inner").addClass("fade-out-inner");
		
	});
})(jQuery)
</script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-slider.js"></script> <script src="js/jquery.stellar.js"></script> 
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script> 
<script src="js/isotope.min.js"></script> 
<script src="js/masonry.js"></script> 
<script src="js/owl.carousel.js"></script> 
<script src="js/wow.js"></script> 
<script src="js/counter.js"></script> 
<script src='js/jquery.themepunch.tools.min.js' type='text/javascript' ></script> <script src='js/jquery.themepunch.revolution.min.js' type='text/javascript' ></script> 
<script src='js/jquery.validate.min.js' type='text/javascript' ></script> 
<script src='js/jquery.form.js' type='text/javascript' ></script> 
<script src='js/contact-form.js' type='text/javascript' ></script> 
<script src='js/settings.js' type='text/javascript' ></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.counterup.min.js"></script>
<script src="js/scripts.js"></script> 
</body>
</html>