<footer>
<div class="container">

    <div class="top-footer">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="box-content"> <i class="ion-navigate"></i>
					<h5>About Us</h5>
					<p>Produk Pompa air DAB dipercaya karena kualitas, disukai karena handal, dipilih karena menguntungkan karena DAB sudah membuktikannya kepada pemakai pompa air DAB di Indonesia dan beberapa sertifikat dunia seperti IQ Net, ESQ dan SNI tentunya.</p>
				</div>
				<br />
				<div class="box-content"> <i class="ion-android-microphone"></i>
					<h5>HEADQUARTER</h5>
					<p>Intirub Business Park, Lt. 2 & 3. Jl. Cililitan Besar No. 454. Jakarta Timur 13650<br />
					Telp. +62 21 4695  1900<br />
					Fax. +62 21 8087 3335</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="box-content"> 
				<h4>Recent Posts</h4>
				<?php
				$result = mysqli_query($con, "SELECT * FROM tr_news ORDER BY n_date DESC LIMIT 1");
				while ($query = mysqli_fetch_assoc($result)) {
				$datee = new dateTime($query['n_date']);
				$datee = $datee->format('F j, Y');
				?>
				<a href="news.php?id=<?php echo $query['n_id']; ?>"> <figure><img width="100%" src="<?php echo $query['n_picture']; ?>"></figure> </a>
				<h5><a style="color:#ffdd00" href="news.php?id=<?php echo $query['n_id']; ?>"><?php echo $query['n_header']; ?></a></h5>
				<p><?php echo $datee; ?></p>
				<?php } ?>
				</div>
            </div>
			<div class="col-md-4 col-sm-12">
			<div class="fb-page" data-href="https://www.facebook.com/dabpompa/" data-tabs="timeline" data-height="325" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/dabpompa/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dabpompa/">DAB Pumps - Indonesia - Original by Italy</a></blockquote></div>
			</div>
        </div>
    </div>
	
	<div class="sub-footer">
		<div class="row">
			<div class="col-xs-12"> <span class="copyright">Copyright © 2017, DAB Pumps Indonesia</span> <span class="creation">Developed by <a href="http://www.bezhoe.com/">BeDeaL</a></span> </div>
		</div>
    </div>
	
</div>
    
    
    
</div>
</footer>
<!-- end footer --> 