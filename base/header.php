<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=635689506530524";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<header>

<nav class="navbar navbar-default" role="navigation" id="nav">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle toggle-menu menu-left push-body" data-toggle="collapse" data-target="#mobile-menu"> <i class="ion-navicon"></i> </button>
        
        <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="Image"></a> </div>
        <div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="mobile-menu">
    
            <ul class="nav navbar-nav navbar-right">
				<li <?php if($page == "home") echo 'class="active"'; ?>><a href="index.php" class="transition">Home</a> </li>
				<li <?php if($page == "about") echo 'class="active"'; ?>><a href="about-us.php" class="transition">About Us</a> </li>
				<li <?php if($page == "product") echo 'class="active"'; ?>><a href="product.php" class="transition">Products</a> </li>
				<li <?php if($page == "news") echo 'class="active"'; ?>><a href="news.php" class="transition">News</a> </li>
				<li <?php if($page == "contact") echo 'class="active"'; ?>><a href="contact-us.php" class="transition">Contact Us</a> </li>
            </ul>
        
        </div>
    </div>
</nav>

</header>